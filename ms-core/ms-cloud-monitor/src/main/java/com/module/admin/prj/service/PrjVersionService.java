package com.module.admin.prj.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.module.admin.prj.pojo.PrjVersion;
import com.system.comm.model.KvEntity;
import com.system.handle.model.ResponseFrame;

/**
 * prj_version的Service
 * @author yuejing
 * @date 2016-10-19 15:55:36
 * @version V1.0.0
 */
@Component
public interface PrjVersionService {
	
	/**
	 * 保存或修改
	 * @param prjVersion
	 * @return
	 */
	public ResponseFrame saveOrUpdate(PrjVersion prjVersion);
	
	/**
	 * 根据prjId获取对象
	 * @param prjId
	 * @return
	 */
	public PrjVersion get(Integer prjId, String version);

	/**
	 * 分页获取对象
	 * @param prjVersion
	 * @return
	 */
	public ResponseFrame pageQuery(PrjVersion prjVersion);
	
	/**
	 * 根据prjId删除对象
	 * @param prjId
	 * @return
	 */
	public ResponseFrame delete(Integer prjId, String version);

	public List<KvEntity> findKvAll();

	public List<PrjVersion> findByPrjId(Integer prjId);
	/**
	 * 修改为发布版本
	 * @param prjId
	 * @param version
	 */
	public void updateRelease(Integer prjId, String version);

	/**
	 * 查找当前机器要定时发布的版本
	 * @param relMsg 
	 * @return
	 */
	public List<PrjVersion> findRelTime(String relMsg);

	/**
	 * 修改项目版本的发布状态
	 * @param prjId
	 * @param version
	 * @param relStatus	[10待发布、20发布中，30发布失败、40发布成功]
	 * @param relMsg
	 */
	public void updateRelTimeStatus(Integer prjId, String version, Integer relStatus,
			String relMsg);

	/**
	 * 修改服务为发布中
	 * @param relMsg
	 */
	public void updateRelTimeWait(String relMsg);
}
