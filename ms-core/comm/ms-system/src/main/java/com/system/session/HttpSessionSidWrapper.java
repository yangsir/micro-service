package com.system.session;

import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.HttpSession;

/**
 * HttpSessionSidWrapper
 * @author yuejing
 * @date 2018年12月26日 下午5:25:13
 */
public class HttpSessionSidWrapper extends HttpSessionWrapper {

	private String sid;

	private Map<Object, Object> map = null;

	public HttpSessionSidWrapper(String sid, HttpSession session) {
		super(session);
		this.sid = sid;
		this.map = SessionService.getInstance().getSession(sid);
	}

	@Override
	public Object getAttribute(String arg0) {
		return this.map.get(arg0);
	}

	@Override
	public Enumeration getAttributeNames() {
		return (new Enumerator(this.map.keySet(), true));
	}

	@Override
	public void invalidate() {
		this.map.clear();
		SessionService.getInstance().removeSession(this.sid);
	}

	@Override
	public void removeAttribute(String arg0) {
		this.map.remove(arg0);
		SessionService.getInstance().saveSession(this.sid, this.map);
	}

	@Override
	public void setAttribute(String arg0, Object arg1) {
		this.map.put(arg0, arg1);
		SessionService.getInstance().saveSession(this.sid, this.map);
	}

}
